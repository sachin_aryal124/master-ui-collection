import 'package:flutter/material.dart';
import 'package:master_ui_collection/core/widgets/bottom_sheet/master_bottom_sheet.dart';

class BottomSheetView extends StatelessWidget {
  const BottomSheetView({super.key});

  void showMasterBottomSheet(BuildContext context) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return const MasterBottomSheet(
          title: 'title',
          centerTitle: true,
          content: Text('this is content'),
          addCancelButton: false,
          addSaveButton: false,
          backgroundColor: Colors.white,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: TextButton(
            onPressed: () => showMasterBottomSheet(context),
            child: const Text('Tap')),
      ),
    );
  }
}
