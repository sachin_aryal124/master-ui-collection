import 'package:flutter/material.dart';
import 'package:master_ui_collection/masterui.dart';

class SkeletonView extends StatelessWidget {
  const SkeletonView({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MasterSkeleton(
                width: 80,
                height: 80,

                borderRadius: BorderRadius.circular(40),
              ),
              const SizedBox(width: 8.0),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const MasterSkeleton(
                      width: 300,
                      height: 20,
                      color: Colors.black12,
                    ),
                    const SizedBox(height: 8.0),
                    MasterSkeleton(
                      width: 300,
                      height: 150,
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    const SizedBox(height: 8.0),
                    const Row(
                      children: [
                        MasterSkeleton(),
                        SizedBox(width: 8.0),
                        MasterSkeleton(),
                        SizedBox(width: 8.0),
                        MasterSkeleton(),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
