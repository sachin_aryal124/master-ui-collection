import 'package:flutter/material.dart';
import 'package:master_ui_collection/masterui.dart';

class MasterTabBarView extends StatelessWidget {
  const MasterTabBarView({super.key});

  @override
  Widget build(BuildContext context) {
    var deoration = <BoxDecoration>[
      const BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10), topRight: Radius.circular(10)),
          color: Colors.pink),
      BoxDecoration(
          gradient:
              const LinearGradient(colors: [Colors.redAccent, Colors.orangeAccent]),
          borderRadius: BorderRadius.circular(50),
          color: Colors.redAccent),
    ];


    var shapeDeoration = <BoxDecoration>[
      const BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10), topRight: Radius.circular(10)),
          color: Colors.pink),
      BoxDecoration(
          gradient:
          const LinearGradient(colors: [Colors.redAccent, Colors.orangeAccent]),
          borderRadius: BorderRadius.circular(50),
          color: Colors.redAccent),
    ];

    List<String> pages = [
      'Home',
      'Settings',
      'Profile',
      'Notifications',
    ];
    return SafeArea(
      child: Scaffold(
        body: SizedBox(
          height: 500,
          width: 600,
          child: Column(
            children: [
              ...deoration
                  .map(
                    (e) => MasterTabBar2(
                      itemCount: pages.length,
                      indicatorHeight: 60,
                      scroolable: false,
                      borderRadius: 2,
                      unselectedLabelColor: Colors.green,
                      indicatorColor: Colors.blueAccent,
                      customIndicator: e,
                      tabBuilder: (BuildContext context, int index) {
                        return Tab(
                          text: pages[index],
                          icon: const Icon(Icons.ac_unit_sharp, size: 20),
                        );
                      },
                      pageBuilder: (BuildContext context, int index) {
                        return Container(
                          height: 100,
                        );
                      },
                      onPositionChange: (int value) {},
                      onScroll: (double value) {},
                      initPosition: 0,
                    ),
                  )
                  .toList()
            ],
          ),
        ),
      ),

      // Column(
      //   children: [
      //
      //     Container(
      //       height: 200,
      //       child: MasterTabBar(
      //         appBarProps: AppBarProps(
      //           title: const Text("Master Tab Bar"),
      //           leading: const Icon(Icons.menu),
      //           actions: [
      //             const Icon(Icons.search),
      //             const SizedBox(width: 16),
      //           ],
      //         ),
      //         tabBarProps: TabBarProps(
      //           indicatorColor: Colors.white,
      //           indicatorWeight: 2,
      //           labelColor: Colors.white,
      //         ),
      //         tabs: const [
      //           Tab(icon: Icon(Icons.home)),
      //           Tab(icon: Icon(Icons.settings)),
      //           Tab(icon: Icon(Icons.person)),
      //           Tab(icon: Icon(Icons.notifications)),
      //         ],
      //         items: pages,
      //
      //         tabBarViewItembuilder: (context, index, item) {
      //           String page = pages[index];
      //           return Expanded(
      //             child: Center(
      //               child: Text(
      //                 page,
      //                 style: Theme.of(context).textTheme.titleLarge,
      //               ),
      //             ),
      //           );
      //         },
      //       ),
      //     ),
      //   ],
      // )
    );
  }
}
