enum MasterWidgetSize { mini,small, medium, large }

extension Value on MasterWidgetSize {
  double get value {
    switch (this) {
      case MasterWidgetSize.mini:
        return 25;
      case MasterWidgetSize.small:
        return 32;
      case MasterWidgetSize.medium:
        return 40;
      case MasterWidgetSize.large:
        return 48;
    }
  }
}
