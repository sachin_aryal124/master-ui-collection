// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

class MasterBottomSheet extends StatelessWidget {
  final String title;
  final Widget content;
  final bool centerTitle;
  final bool addSaveButton;
  final VoidCallback? onSaved;
  final bool addCancelButton;
  final Color backgroundColor;
  const MasterBottomSheet(
      {Key? key,
      required this.title,
      required this.content,
      this.addSaveButton = true,
      this.onSaved,
      this.addCancelButton = true,
      this.centerTitle = false,
      this.backgroundColor = Colors.white})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(16.0),
          topRight: Radius.circular(16.0),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          // Custom Header
          Container(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: centerTitle
                  ? MainAxisAlignment.center
                  : MainAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: const TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
          // Custom Content
          Container(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: content),
          // Custom Buttons
          ButtonBar(
            alignment: MainAxisAlignment.end,
            children: [
              if (addCancelButton)
                TextButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: const Text('Cancel'),
                ),
              if (addSaveButton)
                TextButton(
                  onPressed: onSaved,
                  child: const Text('Save'),
                ),
            ],
          ),
        ],
      ),
    );
  }
}
