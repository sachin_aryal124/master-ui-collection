import 'package:flutter/material.dart';

class MasterCheckbox extends StatefulWidget {
  const MasterCheckbox(
      {Key? key,
      this.width = 24.0,
      this.height = 24.0,
      this.backgroundColor,
      this.borderColor,
      this.iconSize,
      required this.onChanged,
      this.checkColor,
      required this.title,
      this.isLeading = true,
      this.isTrailing = false,
      this.checkWidget = const Icon(Icons.done),
      this.unCheckWidget = const SizedBox(),
      this.borderRadius = 2.0,
      this.isCustom = false,
      this.borderWidth = 1,
      this.selectedTileColor = Colors.transparent})
      : super(key: key);

  final double width;
  final double height;
  final Color? borderColor;
  final Color? backgroundColor;

  // Now you can set the checkmark size of your own
  final double? iconSize;
  final Color? checkColor;
  final Function(bool) onChanged;
  final String title;
  final bool isLeading;
  final bool isTrailing;
  final bool isCustom;
  final double borderWidth;
  final Widget checkWidget;
  final Widget unCheckWidget;
  final double borderRadius;
  final Color selectedTileColor;

  @override
  State<MasterCheckbox> createState() => _MasterCheckboxState();
}

class _MasterCheckboxState extends State<MasterCheckbox> {
  bool isChecked = false;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      tileColor: isChecked ? widget.selectedTileColor : null,
      dense: true,
      onTap: () {
        setState(() => isChecked = !isChecked);
        widget.onChanged.call(isChecked);
      },
      title: Text(widget.title),
      hoverColor: Colors.transparent,
      splashColor: Colors.transparent,
      trailing:
          widget.isTrailing ? _buildCheckWidget() : const SizedBox.shrink(),
      leading: widget.isLeading ? _buildCheckWidget() : const SizedBox.shrink(),
    );
  }

  _buildCheckWidget() {
    if (widget.isCustom) {
      return isChecked ? widget.checkWidget : widget.unCheckWidget;
    } else {
      return Container(
          width: widget.width,
          height: widget.height,
          decoration: BoxDecoration(
            color: widget.backgroundColor,
            border: Border.all(
              color: widget.borderColor ?? Colors.grey.shade500,
              width: widget.borderWidth
            ),
            borderRadius: BorderRadius.circular(widget.borderRadius),
          ),
          child: Center(
            child:
              isChecked ? widget.checkWidget : widget.unCheckWidget,

          ));
    }
  }
}
