import 'package:flutter/material.dart';
import 'package:master_ui_collection/masterui.dart';

class MasterDialog extends StatelessWidget {
  final Color? backgroundColor;
  final double? elevation;
  final ShapeBorder? shape;
  final AlignmentGeometry? alignment;
  final ImageProvider? image;
  final double? imageWidth;
  final double? imageHeight;
  final BoxFit? imageFit;
  final String title;
  final TextStyle? titleStyle;
  final String? description;
  final TextStyle? descriptionStyle;
  final List<Widget>? actions;
  final MainAxisAlignment? actionsMainAxisAlignment;
  const MasterDialog({
    super.key,
    // dialog
    this.backgroundColor,
    this.elevation,
    this.shape,
    this.alignment,
    // dialog content
    required this.title,
    this.titleStyle,
    this.image,
    this.imageWidth,
    this.imageHeight,
    this.imageFit,
    this.description,
    this.descriptionStyle,
    this.actions,
    this.actionsMainAxisAlignment,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: backgroundColor,
      elevation: elevation,
      shape: shape,
      alignment: alignment,
      child: buildDialogContent(context),
    );
  }

  Widget buildDialogContent(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        if (image != null)
          Image(
            image: image!,
            width: imageWidth ?? double.infinity,
            height: imageHeight ?? 200,
            fit: imageFit ?? BoxFit.cover,
          ),
        Padding(
          padding: const EdgeInsets.all(spacing16),
          child: Column(
            children: [
              Text(title,
                  style: Theme.of(context)
                      .textTheme
                      .titleLarge
                      ?.merge(titleStyle)),
              if (description != null) const SizedBox(height: spacing8),
              if (description != null)
                Text(
                  description!,
                  style: Theme.of(context)
                      .textTheme
                      .bodyMedium
                      ?.copyWith(
                        fontSize: 16,
                        color: Theme.of(context)
                            .colorScheme
                            .onSurface
                            .withOpacity(0.6),
                      )
                      .merge(descriptionStyle),
                  textAlign: TextAlign.center,
                ),
              if (actions != null)
                Column(
                  children: [
                    const SizedBox(height: spacing4),
                    const Divider(),
                    const SizedBox(height: spacing4),
                    Row(
                      mainAxisAlignment:
                          actionsMainAxisAlignment ?? MainAxisAlignment.center,
                      children: actions!,
                    ),
                  ],
                ),
            ],
          ),
        )
      ],
    );
  }
}
