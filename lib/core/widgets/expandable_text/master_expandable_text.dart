import 'package:dfunc/dfunc.dart';
import 'package:flutter/material.dart';

class MasterExpandableText extends StatefulWidget {
  final String text;
  final int maxLines;
  final TextOverflow overflow;
  final TextStyle? textStyle;
  final Locale? locale;
  final TextAlign? textAlign;
  final Widget? showMore;
  final Widget? showLess;
  final String showMoreText;
  final String showLessText;
  final bool expandOnTextTap;
  final Curve animationCurve;
  final int seconds;
  final bool left;

  const MasterExpandableText({
    Key? key,
    required this.text,
    this.maxLines = 3,
    this.overflow = TextOverflow.ellipsis,
    this.textStyle,
    this.locale,
    this.textAlign,
    this.showMore,
    this.showLess,
    this.showMoreText = 'Read more',
    this.showLessText = 'Read less',
    this.expandOnTextTap = true,
    this.animationCurve = Curves.easeInOut,
    this.seconds = 1,
    this.left = false,
  }) : super(key: key);

  @override
  _MasterExpandableTextState createState() => _MasterExpandableTextState();
}

class _MasterExpandableTextState extends State<MasterExpandableText>
    with TickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;
  bool _isExpanded = false;

  void _toggleExpanded() {
    setState(() {
      _isExpanded = !_isExpanded;
    });

    if (_isExpanded) {
      _controller.forward();
    } else {
      _controller.reverse();
    }
  }

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration:  Duration(seconds: widget.seconds),
      vsync: this,
    );
    _animation = CurvedAnimation(
      parent: _controller,
      curve: widget.animationCurve,
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: widget.left?CrossAxisAlignment.start:CrossAxisAlignment.end,
      mainAxisSize: MainAxisSize.min,
      children: [
        GestureDetector(
          onTap: widget.expandOnTextTap ? _toggleExpanded : null,
          child: AnimatedSize(
            duration: const Duration(milliseconds: 300),
            alignment: Alignment.topLeft,
            curve: Curves.fastOutSlowIn,
            child: ConstrainedBox(
              constraints: _isExpanded
                  ? const BoxConstraints()
                  : BoxConstraints(maxHeight: widget.maxLines * 16.0),
              child: Text(
                widget.text,
                maxLines: _isExpanded ? null : widget.maxLines,
                overflow: _isExpanded ? null : widget.overflow,
                style: widget.textStyle,
                locale: widget.locale,
                textAlign: widget.textAlign,
              ),
            ),
          ),
        ),
        const SizedBox(height: 4.0),
        if (!_isExpanded)
          widget.showMore != null
              ? GestureDetector(
                  onTap: _toggleExpanded,
                  child: widget.showMore!,
                )
              : TextButton(
                  onPressed: _toggleExpanded,
                  child: Text(widget.showMoreText),
                ),
        if (_isExpanded)
          widget.showLess != null
              ? GestureDetector(
                  onTap: _toggleExpanded,
                  child: widget.showLess!,
                )
              : TextButton(
                  onPressed: _toggleExpanded,
                  child: Text(widget.showLessText),
                ),
      ],
    );
  }
}
