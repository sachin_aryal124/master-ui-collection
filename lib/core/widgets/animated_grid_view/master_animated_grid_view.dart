import 'package:flutter/material.dart';
import 'package:master_ui_collection/masterui.dart';

class MasterAnimatedGridView<T> extends StatefulWidget {
  final List<T> items;
  final Widget Function(BuildContext context, int index) itemBuilder;
  final Duration duration;
  final AnimationVariant animationVariant;
  final bool once;
  final int crossAxisCount;
  final double mainAxisSpacing;
  final double crossAxisSpacing;
  final double childAspectRatio;
  final double? mainAxisExtent;
  final Curve animationCurve;

  const MasterAnimatedGridView({
    super.key,
    required this.items,
    required this.itemBuilder,
    required this.crossAxisCount,
    this.duration = const Duration(milliseconds: 500),
    this.animationVariant = AnimationVariant.scale,
    this.once = false,
    this.mainAxisSpacing = 0.0,
    this.crossAxisSpacing = 0.0,
    this.childAspectRatio = 1.0,
    this.mainAxisExtent,
    this.animationCurve = Curves.ease,

  });

  @override
  State<MasterAnimatedGridView<T>> createState() => _MasterAnimatedGridViewState<T>();
}

class _MasterAnimatedGridViewState<T> extends State<MasterAnimatedGridView<T>> {


  final _listItems = <T>[];
  final GlobalKey<AnimatedGridState> _listKey = GlobalKey();

  @override
  void initState() {
    _loadItems();
    super.initState();

  }

  void _loadItems() {
    // fetching data from web api, db...
    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      for (var i = 0; i < widget.items.length; i++) {
        await Future.delayed(widget.duration);
        _listItems.add(widget.items[i]);
        _listKey.currentState?.insertItem(_listItems.length - 1);
      }
      print(_listItems.length);
    });
  }
  @override
  Widget build(BuildContext context) {
    return
      AnimatedGrid(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: widget.crossAxisCount,
        mainAxisSpacing: widget.mainAxisSpacing,
        crossAxisSpacing: widget.crossAxisSpacing,
        mainAxisExtent: widget.mainAxisExtent,
      ),
      key: _listKey,
      padding: const EdgeInsets.only(top: 10),
      initialItemCount: _listItems.length,
      itemBuilder: (context, index, animation) {
        return MasterAnimatedListViewItem(
          duration: widget.duration,
          animationVariant: widget.animationVariant,
          once: widget.once,
          animaionCurve: widget.animationCurve,
          child: widget.itemBuilder(context, index),
        );
      },
    );
  }
}
