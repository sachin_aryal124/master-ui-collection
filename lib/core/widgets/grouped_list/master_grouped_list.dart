import 'package:flutter/material.dart';

class MasterGroupedList<T> extends StatefulWidget {
  final List<GroupedListItem<T>> items;
  final Widget Function(BuildContext context, T item) groupItemBuilder;
  final Widget Function(BuildContext context, String title)? groupHeaderBuilder;
  const MasterGroupedList({
    super.key,
    required this.items,
    required this.groupItemBuilder,
    this.groupHeaderBuilder,
  });

  @override
  State<MasterGroupedList<T>> createState() => _MasterGroupedListState<T>();
}

class _MasterGroupedListState<T> extends State<MasterGroupedList<T>> {
  final ScrollController _scrollController = ScrollController();
  final int _stickyHeaderIndex = 0;
  final double _itemHeight = 300;

  void _updateStickyHeaderTitle() {
    double offset = _scrollController.offset;
    int index = (offset / (_itemHeight)).floor();
    print('index is $index');
  }

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_updateStickyHeaderTitle);
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.removeListener(_updateStickyHeaderTitle);
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: double.infinity,
          child: Container(
            color: Colors.green,
            padding: const EdgeInsets.all(16.0),
            child: Text(widget.items[_stickyHeaderIndex].title),
          ),
        ),
        Expanded(
          child: ListView.builder(
            controller: _scrollController,
            itemBuilder: (context, index) {
              final item = widget.items[index];
              return Column(
                children: [
                  if (_stickyHeaderIndex != index)
                    widget.groupHeaderBuilder != null
                        ? widget.groupHeaderBuilder!(context, item.title)
                        : Text(item.title),
                  widget.groupItemBuilder(context, item.item),
                ],
              );
            },
            itemCount: widget.items.length,
          ),
        ),
      ],
    );
  }
}

class GroupedListItem<T> {
  final String title;
  final T item;

  const GroupedListItem({
    required this.title,
    required this.item,
  });
}
