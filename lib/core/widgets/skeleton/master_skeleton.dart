import 'package:flutter/material.dart';

class MasterSkeleton extends StatefulWidget {
  final double? width;
  final double? height;
  final BorderRadiusGeometry? borderRadius;
  final Color? color;
  final AnimationController? animationController;
  final Animation<double>? animation;
  final Duration? duration;
  final Widget? child;
  const MasterSkeleton({
    super.key,
    this.width,
    this.height,
    this.borderRadius,
    this.color,
    this.animationController,
    this.animation,
    this.duration,
    this.child,
  });

  @override
  State<MasterSkeleton> createState() => _MasterSkeletonState();
}

class _MasterSkeletonState extends State<MasterSkeleton>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    _animationController = widget.animationController ??
        AnimationController(
          vsync: this,
          duration: widget.duration ?? const Duration(seconds: 1),
        )
      ..repeat(reverse: true);
    _animation = widget.animation ??
        Tween<double>(begin: 0.4, end: 1.0).animate(
          CurvedAnimation(
            parent: _animationController,
            curve: Curves.easeIn,
          ),
        );
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animation,
      builder: (context, child) {
        return Opacity(
          opacity: _animation.value,
          child: widget.child ??
              Container(
                width: widget.width ?? 60,
                height: widget.height ?? 20,
                decoration: BoxDecoration(
                  borderRadius:
                      widget.borderRadius ?? BorderRadius.circular(8.0),
                  color: widget.color ?? Colors.grey[300],
                ),
              ),
        );
      },
    );
  }
}
