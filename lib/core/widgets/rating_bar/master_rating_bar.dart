import 'package:flutter/material.dart';

class MasterRatingBar extends StatefulWidget {
  final double rating;
  final int maxRating;

  final double size;
  final Color filledColor;
  final Color emptyColor;

  final IconData filledIcon;
  final IconData halfFilledIcon;
  final IconData emptyIcon;

  final bool isDisabled;
  final void Function(double rating)? onRatingChanged;

  const MasterRatingBar({
    super.key,
    required this.rating,
    this.maxRating = 5,
    this.size = 30.0,
    this.filledColor = Colors.orange,
    this.emptyColor = Colors.grey,
    this.emptyIcon = Icons.star_outline,
    this.filledIcon = Icons.star,
    this.halfFilledIcon = Icons.star_half,
    this.isDisabled = false,
    this.onRatingChanged,
  }) : assert(rating <= maxRating, 'Rating cannot be greater than maxRating');

  @override
  State<MasterRatingBar> createState() => _MasterRatingBarState();
}

class _MasterRatingBarState extends State<MasterRatingBar> {
  late double _rating;

  @override
  void initState() {
    _rating = widget.rating;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: List.generate(
        widget.maxRating,
        (index) {
          if (index < _rating.floor()) {
            return buildRatingIcon(
              widget.filledIcon,
              widget.filledColor,
              index,
            );
          }
          if (index < _rating.ceil()) {
            return buildRatingIcon(
              widget.halfFilledIcon,
              widget.filledColor,
              index,
            );
          }
          return buildRatingIcon(
            widget.emptyIcon,
            widget.emptyColor,
            index,
          );
        },
      ),
    );
  }

  Widget buildRatingIcon(
    IconData icon,
    Color color,
    int iconIndex,
  ) {
    return GestureDetector(
      onTap: widget.isDisabled
          ? null
          : () {
              setState(() {
                _rating = iconIndex + 1.0;
                if (widget.onRatingChanged != null) {
                  widget.onRatingChanged!(
                    _createReadableRating(_rating),
                  );
                }
              });
            },
      onHorizontalDragUpdate: widget.isDisabled
          ? null
          : (details) {
              setState(() {
                double dragSpeed = 40;
                double newRating = _rating + details.delta.dx / dragSpeed;
                _rating = newRating.clamp(0.0, widget.maxRating.toDouble());
                if (widget.onRatingChanged != null) {
                  widget.onRatingChanged!(
                    _createReadableRating(_rating),
                  );
                }
              });
            },
      child: Icon(
        icon,
        size: widget.size,
        color: color,
      ),
    );
  }

  double _createReadableRating(double rating) {
    return (rating * 2).round() / 2;
  }
}
