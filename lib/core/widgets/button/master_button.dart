import 'package:flutter/material.dart';

import '../../widget_size.dart';

class MasterButton extends StatelessWidget {
  final String buttonText;
  final MasterWidgetSize size;
  final EdgeInsets padding;
  final GestureTapCallback? onPressed;
  final Duration duration;
  final Widget leadingWidget;
  final Widget trailingWidget;
  final Color? backgroundColor;
  final Color borderColor;
  final Color? textColor;
  final double? minWidth;
  final TextStyle textStyle;
  final double elevation;
  final double radius;
  final bool iconOnly;
  final Widget? icon;

  const MasterButton({
    Key? key,
    this.buttonText = "button",
    this.onPressed,
    this.minWidth,
    this.padding = const EdgeInsets.all(10),
    this.duration = const Duration(seconds: 5),
    this.leadingWidget = const SizedBox.shrink(),
    this.textColor,
    this.elevation = 0,
    this.radius = 4,
    this.icon,
    this.iconOnly = false,
    this.borderColor = Colors.transparent,
    this.backgroundColor = Colors.black,
    this.trailingWidget = const SizedBox.shrink(),
    this.textStyle = const TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.bold,
        letterSpacing: 2.2,
        overflow: TextOverflow.ellipsis),
    this.size = MasterWidgetSize.medium,
  }) : super(key: key);

  Widget _buildIcon(Widget icon) => SizedBox(
        child: icon,
      );

  @override
  Widget build(BuildContext context) => Container(
      margin: padding,
      height: size.value,
      width: minWidth,
      child: ElevatedButton(
        onPressed: onPressed,
        style: ButtonStyle(
            padding: const MaterialStatePropertyAll<EdgeInsetsGeometry>(
                EdgeInsets.zero),
            elevation: MaterialStateProperty.all<double>(elevation),
            backgroundColor: MaterialStateProperty.all<Color>(backgroundColor!),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(radius),
                    side: BorderSide(color: borderColor)))),
        child: iconOnly
            ? Container(
                child: icon,
              )
            : Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _buildIcon(leadingWidget),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: DefaultTextStyle.merge(
                          child: Text(buttonText),
                          style: textStyle.copyWith(color: textColor)),
                    ),
                    _buildIcon(trailingWidget),
                  ],
                ),
              ),
      ));
}
