const double spacing0 = 0;
const double spacing2 = 2;
const double spacing4 = 4;
const double spacing8 = 8;
const double spacing16 = 16;
const double spacing24 = 24;
const double spacing32 = 32;
const double spacing40 = 40;
const double spacing56 = 56;
const double spacing72 = 72;
const double spacing96 = 96;
